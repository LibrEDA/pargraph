<!--
SPDX-FileCopyrightText: 2023 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# ParGraph - Parallel Graph Processing library

Pargraph is a yet simple and humble library for parallel processing of `petgraph` data structures.

## Testing

Run basic tests with `cargo test`.

Properly testing concurrent data structures is difficult. This crate
uses the `loom` crate for testing internal synchronization primitives.
This tests must be run separately with:
```rust
RUSTFLAGS="--cfg loom" cargo test
```
